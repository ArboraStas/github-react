import React from 'react';
import ReactDOM, {render} from 'react-dom';
import qajax from 'qajax';
import UserList from './app/components/UserList';
import Dashboard from './app/components/Dashboard';
import DetailedUser from './app/components/DetailedUser';
import {Router, Route, IndexRoute, Redirect, IndexRedirect, useRouterHistory} from 'react-router';
import createHashHistory from 'history/lib/createHashHistory';

const appHistory = useRouterHistory(createHashHistory)({queryKey: false});

appHistory.listen(() => {
    const {location} = window;

    if (location && location.pathname && location.hash && window.ga) {
        window.ga('send', 'pageview', location.pathname + location.hash);
    }
});

export default class App extends React.Component {
    constructor(props) {
        super(props);
    }
}

render(<Router history={appHistory}>
    <Route path="/" component={Dashboard}>
        <IndexRoute component={UserList}/>
        <Route path="users/:userId"
               component={UserList}>
        </Route>
        <Route path="user/:userId"
               component={DetailedUser}/>
    </Route>
</Router>, document.getElementById('app'));

