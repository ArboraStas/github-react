var gulp         = require('gulp'),
    less         = require('gulp-less'),
    csscomb      = require('gulp-csscomb'),
    plumber      = require('gulp-plumber'),
    autoprefixer = require('gulp-autoprefixer');
    // imagemin     = require('gulp-imagemin'),
    // pngquant     = require('imagemin-pngquant'),
    // imageminJpegtran = require('imagemin-jpegtran'),
    // browserSync  = require('browser-sync').create();
var path = {
    build: {
        html: 'web/markup/build/',
        js: 'web/markup/build/js/',
        css: 'build/css/',
        img: 'web/markup/build/images/',
        fonts: 'web/markup/build/fonts/'
    },
    src: {
        html: 'web/markup/src/**/*.html',
        js: 'web/markup/src/js/*.js',
        css: 'css/style.less',
        img: 'web/markup/src/images/**/*.*',
        fonts: 'web/markup/src/fonts/**/*.*'
    },
    watch: {
        html: 'web/markup/src/**/*.html',
        js: 'web/markup/src/js/*.js',
        css: 'css/*.less',
        img: 'web/markup/src/images/**/*.*',
        fonts: 'web/markup/src/fonts/**/*.*'
    }
};

// gulp.task('browser-sync', function() {
//     browserSync.init({
//         proxy: "recap.dev",
//         open: false
//     });
// });

gulp.task('html', function() {
    gulp.src(path.src.html)
        .pipe(gulp.dest(path.build.html))
        // .pipe(browserSync.stream());
});

gulp.task('css', function() {
    gulp.src(path.src.css)
        .pipe(plumber())
        .pipe(less())
        .pipe(autoprefixer({
            browsers: ['last 2 versions'],
            cascade: false,
            remove: false
        }))
        .pipe(csscomb())
        .pipe(gulp.dest(path.build.css))
        // .pipe(browserSync.stream());
});

gulp.task('js', function() {
    gulp.src(path.src.js)
        .pipe(gulp.dest(path.build.js))
        // .pipe(browserSync.stream());
});

gulp.task('images', function() {
    gulp.src(path.src.img)
        // imagemin({
        //     progressive: true,
        //     svgoPlugins: [{removeViewBox: false}],
        //     use: [pngquant(), imageminJpegtran()]
        // })
        .pipe(gulp.dest(path.build.img));
});

gulp.task('fonts', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
        // .pipe(browserSync.stream());
});

gulp.task('watch', function() {
    gulp.watch([path.watch.html], ['html']);
    gulp.watch([path.watch.css], ['css']);
    gulp.watch([path.watch.img], ['images']);
    gulp.watch([path.watch.fonts], ['fonts']);
    gulp.watch([path.watch.js], ['js']);
});

gulp.task('build', [
    'html',
    'js',
    'css',
    'images',
    'fonts',
    'watch'
]);

gulp.task('default', ['watch', 'build']);