import React, {PropTypes} from 'react';

export default class User extends React.Component {
    componentDidMount() {
        const {params} = this.props;

        if (params) {
            console.log("load user with id " + params.userId);
        }
    }

    goToUser(id) {
        const {user} = this.props;

        this.context.router.push(`user/${user.id}`);
    }

    handleClickOnProfileLink(e) {
        e.stopPropagation();
    }

    render() {
        const {user = {}} = this.props;

        return (
            <div className='b-user-list__item b-user' onClick={this.goToUser.bind(this)}>
                <img className='b-user__avatar' src={user.avatar_url} alt="" width="100px" height="100px"/>
                <p className='b-user__login'>{user.login}</p>
                <a className='b-user__profile-link' href={user.html_url} target='_blank' onClick={this.handleClickOnProfileLink.bind(this)}>Profile</a>
            </div>
        );
    }
}

User.contextTypes = {
    router: PropTypes.object.isRequired
};
