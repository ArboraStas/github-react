import React, {PropTypes} from 'react';
import qajax from 'qajax';
import {UsersStore, UserActions} from '../stores/UsersStore';
import User from './User';

export default class UserList extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            users: null,
            loading: true
        };

        this.unsubscribeUsersStore = UsersStore.listen(this.onUsersLoad.bind(this));
    }

    componentWillUnmount() {
        this.unsubscribeUsersStore();
    }

    componentDidMount() {
        const {params} = this.props;
        UsersStore.showAllUsers(params.userId);
    }

    onUsersLoad(data) {
        this.setState({
            users: data.users,
            loading: data.loading
        });
    }

    loadMore() {
        const {users} = this.state;
        const lastUserId = users[29].id;

        this.context.router.push(`users/${users[29].id}`);
        UsersStore.showAllUsers(lastUserId);
    }

    render() {
        const {users, loading} = this.state;

        return (
            <div className='b-user-list'>
                {!loading && users ? users.map(user => {
                    return <User key={user.id} user={user}/>
                }) : 'Loading'}

                <div className='b-button' onClick={this.loadMore.bind(this)}>Next users</div>
            </div>
        );
    }
}

UserList.contextTypes = {
    router: PropTypes.object.isRequired
};
