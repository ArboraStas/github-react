import React, {Children, cloneElement} from 'react';

export default class Dashboard extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            users: null
        };
    }

    render() {
        let childrenWithProps = Children.map(this.props.children, child => cloneElement(child, {
            users: child
        }));

        return (
            <div className='b-app'>
                <header className='b-header'>Github Users</header>
                {childrenWithProps}
                <footer className='b-footer'>Arbora Stas</footer>
            </div>
        );
    }
}
