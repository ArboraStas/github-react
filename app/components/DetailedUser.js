import React, {PropTypes} from 'react';
import {UsersStore, UserActions} from '../stores/UsersStore';

export default class DetailedUser extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            user: null,
            loading: true
        };

        this.unsubscribeUserStore = UsersStore.listen(this.onUserLoad.bind(this));
    }

    componentWillUnmount() {
        this.unsubscribeUserStore();
    }

    componentDidMount() {
        const {params} = this.props;

        UsersStore.showUser(params.userId);
    }

    onUserLoad(data) {
        this.setState({
            user: data.user,
            loading: data.loading
        });
    }

    render() {
        const {user, loading} = this.state;

        return (
            <div className='b-detailed-user'>
                {!loading && user ?
                    <div className='b-detailed-user__information'>
                        <img src={user.avatar_url} />
                        {user.name ? <p>Name: {user.name}</p> : null}
                        {user.followers ? <p>Followers: {user.followers}</p> : null}
                        {user.following ? <p>Following: {user.following}</p> : null}
                        {user.created_at ? <p>Created at: {user.created_at}</p> : null}
                        {user.company ? <p>Company: {user.company}</p> : null}
                        {user.email ? <p>Email: {user.email}</p> : null}
                        {user.location ? <p>Location: {user.location}</p> : null}
                        {user.blog ? <p>Blog: <a href={user.blog} target='_blank'>{user.blog}</a></p> : null}
                        {user.bio ? <p>Bio: {user.bio}</p> : null}
                    </div> : 'Loading'}
            </div>
        );
    }
}
