import 'whatwg-fetch';

class Users {
    static showUsers(lastUserId) {
        return fetch(`https://api.github.com/users?since=${lastUserId}`)
            .then(function(response) {
                return response.json()
            });
    }
    static showUser(id) {
        return fetch(`https://api.github.com/user/${id}`)
            .then(function(response) {
                return response.json()
            });
    }
}

export default {Users};