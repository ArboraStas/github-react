import Reflux from 'reflux';
import Api from '../api/Api';
import _ from 'lodash';

export const UsersActions = Reflux.createActions([
    'showAllUsers',
    'showUser'
]);

export let UsersStore = Reflux.createStore({
    init() {
        this.listenToMany(UsersActions);

        this.users = null;
    },

    showAllUsers(lastUserId) {
        this.trigger({
            users: null,
            loading: true
        });

        Api.Users.showUsers(lastUserId).then(users => {
            this.users = users;
            this.trigger({
                users: this.users,
                loading: false
            });
        }).catch(error => console.log(error));
    },
    
    showUser(id) {
        this.trigger({
            user: null,
            loading: true
        });

        Api.Users.showUser(id).then(user => {
            this.user = user;
            this.trigger({
                user: this.user,
                loading: false
            });
        }).catch(error => console.log(error));
    }
});
